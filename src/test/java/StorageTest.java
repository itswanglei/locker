import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StorageTest {
    @Test
    public void should_get_receipt_when_save_bag() {
        Bag bag = new Bag();
        Storage storage = new Storage(1);
        Ticket ticket = storage.save(bag);
        assertNotNull(ticket);
    }

    @Test
    public void should_get_bag_by_receipt() {
        Bag bag = new Bag();
        Storage storage = new Storage(1);
        Ticket ticket = storage.save(bag);
        Bag myBag = storage.get(ticket);
        assertEquals(bag, myBag);
    }

    @Test
    public void should_get_null_when_save_null() {
        Storage storage = new Storage(1);
        Ticket ticket = storage.save(null);
        Bag bag = storage.get(ticket);
        assertNull(bag);
    }

    @Test
    public void should_get_second_bag_when_save_two_bags() {
        Bag bag1 = new Bag();
        Bag bag2 = new Bag();
        Storage storage = new Storage(2);
        Ticket ticket1 = storage.save(bag1);
        Ticket ticket2 = storage.save(bag2);
        Bag myBag = storage.get(ticket2);
        assertEquals(bag2, myBag);
    }

    @Test
    public void should_catch_exception_when_storage_is_out_of_capacity() {
        Storage storage = new Storage(0);
        RuntimeException e = assertThrows(RuntimeException.class, () -> storage.save(new Bag()));
        assertEquals("Out of capacity!", e.getMessage());
    }
}
