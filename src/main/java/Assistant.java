public class Assistant {
    private Storage storage;

    public Assistant(Storage storage) {
        this.storage = storage;
    }

    public Ticket save(Bag bag) {
        return storage.save(bag);
    }

    public Bag get(Ticket ticket) {
        return storage.get(ticket);
    }
}
